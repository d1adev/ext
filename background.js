var c = 0;

function main() {
    $.getJSON('https://api.twitch.tv/kraken/streams/zephimir', function (r) {
        if (r["stream"] != null) {
            if (c == 0) {
                chrome.notifications.create('n', {
                    type: "basic",
                    title: 'Online',
                    message: 'Zephimir est online !',
                    iconUrl: 'zephimir-profile_image-cec3c96846be15a7-300x300.jpeg'
                });
                chrome.browserAction.setTitle({title: 'Zephimir est online !'});
                chrome.browserAction.setIcon({"path": "zephimir-profile_image-cec3c96846be15a7-300x300.jpeg"});
                c = 1;
            }
        }
    });
}

chrome.browserAction.setTitle({title: 'Zephimir est offline !'});
chrome.browserAction.setIcon({"path": "icon_offline.thumbnail.png"});

chrome.browserAction.onClicked.addListener(function (tab) {
    chrome.tabs.create({
        url: 'http://www.twitch.tv/zephimir'
    });
});

chrome.notifications.onClicked.addListener(function (tab) {
    chrome.tabs.create({
        url: 'http://www.twitch.tv/zephimir'
    });
});


main();
setInterval(main, 60000);